export default {
  // Functions return fixtures
  getCocktailList: () => ({
    ok: true,
    data: require('../Fixtures/CocktailList.json'),
  }),
};
