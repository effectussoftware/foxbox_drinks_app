import apisauce from 'apisauce';

const create = (baseURL = 'http://www.thecocktaildb.com/api/json/v1/1/') => {
  const api = apisauce.create({
    baseURL,
    headers: {},
    timeout: 10000,
  });
  const getCocktailList = () => api.get('filter.php?g=Cocktail_glass');
  const getCocktailDetails = idDrink => api.get(`lookup.php?i=${idDrink}`);
  return {
    getCocktailList,
    getCocktailDetails,
  };
};

export default {
  create,
};
