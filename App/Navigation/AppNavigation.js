import { createStackNavigator, createAppContainer } from 'react-navigation';
import CocktailListScreen from '../Screens/CocktailList';

const PrimaryNav = createStackNavigator(
  {
    CocktailListScreen: { screen: CocktailListScreen },
  },
  {
    // Default config for all screens
    headerMode: 'none',
    initialRouteName: 'CocktailListScreen',
  },
);

export default createAppContainer(PrimaryNav);
