import React, { useEffect, useState } from 'react';
import { View, StatusBar, StyleSheet } from 'react-native';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import ReduxNavigation from '../Navigation/ReduxNavigation';
import StartupActions from '../Redux/StartupRedux';
import ReduxPersist from '../Config/ReduxPersist';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const RootContainer = ({ startup }) => {
  const [initialized, setInitialized] = useState(false);

  useEffect(() => {
    if (!initialized) {
      if (!ReduxPersist.active) {
        startup();
      }
    }
    setInitialized(true);
  });

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" />
      <ReduxNavigation />
    </View>
  );
};

RootContainer.propTypes = {
  startup: func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  startup: () => dispatch(StartupActions.startup()),
});

export default connect(
  null,
  mapDispatchToProps,
)(RootContainer);
