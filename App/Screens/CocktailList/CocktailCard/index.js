import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import {
  View, Text, Image, StyleSheet,
} from 'react-native';
import { func } from 'prop-types';
import CocktailListActions from '../duck/redux';
import { drinkPropType } from '../../../Utils/PropTypes';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderRadius: 3,
    marginHorizontal: 10,
    marginVertical: 5,
    backgroundColor: 'white',
    padding: 10,
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    zIndex: 10,
  },
  textContainer: { flex: 1 },
  title: { fontSize: 24 },
  image: { width: 125, height: 130, borderRadius: 3 },
});

const CocktailCard = ({ item, getCocktail }) => {
  const [initialized, setInitialized] = useState(false);

  useEffect(() => {
    if (!initialized && !Object.prototype.hasOwnProperty.call(item, 'strIngredient1')) {
      getCocktail(item.idDrink);
    }
    setInitialized(true);
  });

  const renderIngredients = () => {
    const INGREDIENTS_TO_DISPLAY = 3;
    const drinkEntries = Object.entries(item);
    const drinkIngredients = drinkEntries.filter(
      entry => entry[0].startsWith('strIngredient') && entry[1],
    );
    return drinkIngredients.map((ingredient, i) => {
      if (i < INGREDIENTS_TO_DISPLAY) {
        return <Text key={ingredient[0]}>{ingredient[1]}</Text>;
      }
      if (i === INGREDIENTS_TO_DISPLAY) {
        const remainingIngredients = drinkIngredients.length - INGREDIENTS_TO_DISPLAY;
        return (
          <Text key={ingredient[0]}>
            {`y ${remainingIngredients} ingrediente${remainingIngredients > 1 ? 's' : ''} mas`}
          </Text>
        );
      }
      return null;
    });
  };

  return (
    <View style={styles.container}>
      <View style={styles.textContainer}>
        <Text style={styles.title}>{item.strDrink}</Text>
        {renderIngredients(item)}
      </View>
      <Image source={{ uri: item.strDrinkThumb }} style={styles.image} />
    </View>
  );
};

CocktailCard.propTypes = {
  item: drinkPropType.isRequired,
  getCocktail: func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  getCocktail: id => dispatch(CocktailListActions.getCocktailDetailsRequest(id)),
});

export default connect(
  null,
  mapDispatchToProps,
)(CocktailCard);
