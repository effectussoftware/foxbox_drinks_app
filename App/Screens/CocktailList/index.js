import React, { useEffect, useState } from 'react';
import {
  View, Text, ActivityIndicator, StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import { FlatList } from 'react-native-gesture-handler';

import { func } from 'prop-types';
import { cocktailListPropType } from '../../Utils/PropTypes';
import CocktailCard from './CocktailCard';
import CocktailListActions from './duck/redux';

const styles = StyleSheet.create({
  container: { backgroundColor: 'rgb(0, 192, 212)', flex: 1 },
  headerContainer: { paddingTop: 25, paddingBottom: 10 },
  header: { fontSize: 18, color: 'white', textAlign: 'center' },
  flatlistContainer: { flex: 1 },
  activityIndicator: { paddingVertical: 20 },
});

const CocktailList = ({ getCocktailList, cocktailList }) => {
  const [initialized, setInitialized] = useState(false);
  const [displayLoading, setDisplayLoading] = useState(true);

  useEffect(() => {
    if (!initialized) {
      getCocktailList();
    }
    setInitialized(true);
  });

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.header}>Random Drink 0.1</Text>
      </View>
      <FlatList
        style={styles.flatlistContainer}
        data={cocktailList}
        renderItem={({ item }) => <CocktailCard item={item} />}
        keyExtractor={item => item.idDrink}
        ListFooterComponent={() => displayLoading && <ActivityIndicator style={styles.activityIndicator} size="large" />
        }
        onEndReached={() => setDisplayLoading(false)}
      />
    </View>
  );
};

CocktailList.propTypes = {
  cocktailList: cocktailListPropType.isRequired,
  getCocktailList: func.isRequired,
};

const mapStateToProps = state => ({
  cocktailList: state.cocktailList.list,
});

const mapDispatchToProps = dispatch => ({
  getCocktailList: () => dispatch(CocktailListActions.getCocktailListRequest()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CocktailList);
