import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable';

const { Types, Creators } = createActions({
  getCocktailListRequest: [],
  getCocktailListSuccess: ['list'],
  getCocktailListFailure: ['error'],
  getCocktailDetailsRequest: ['cocktailId'],
  getCocktailDetailsSuccess: ['cocktailDetails'],
  getCocktailDetailsFailure: ['error'],
});

export const CocktailListTypes = Types;
export default Creators;

export const INITIAL_STATE = Immutable({
  list: null,
  error: false,
});

export const success = (state, { list }) => state.merge({ list, error: false });

export const failure = state => state.merge({ error: true });

export const cocktailDetailsSuccess = (state, { cocktailDetails }) => ({
  ...state,
  // eslint-disable-next-line max-len
  list: state.list.map(cocktail => (cocktail.idDrink === cocktailDetails.idDrink ? cocktailDetails : cocktail)),
});

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_COCKTAIL_LIST_SUCCESS]: success,
  [Types.GET_COCKTAIL_LIST_FAILURE]: failure,
  [Types.GET_COCKTAIL_DETAILS_SUCCESS]: cocktailDetailsSuccess,
  [Types.GET_COCKTAIL_LIST_FAILURE]: failure,
});
