import {
  call, put, takeEvery, all,
} from 'redux-saga/effects';
import CocktailListActions, { CocktailListTypes } from './redux';

export function* getCocktailList(api) {
  try {
    const response = yield call(api.getCocktailList);
    const list = response.data.drinks;
    yield put(CocktailListActions.getCocktailListSuccess(list));
  } catch (error) {
    yield put(CocktailListActions.getCocktailListFailure(error));
  }
}

export function* getCocktailDetails(api, { cocktailId }) {
  try {
    const response = yield call(api.getCocktailDetails, cocktailId);
    const [drinkDetails] = response.data.drinks;
    yield put(CocktailListActions.getCocktailDetailsSuccess(drinkDetails));
  } catch (error) {
    yield put(CocktailListActions.getCocktailDetailsFailure(error));
  }
}

export default function* CocktailListSaga(api) {
  yield all([
    takeEvery(CocktailListTypes.GET_COCKTAIL_LIST_REQUEST, getCocktailList, api),
    takeEvery(CocktailListTypes.GET_COCKTAIL_DETAILS_REQUEST, getCocktailDetails, api),
  ]);
}
